package alfred;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.core.events.message.react.MessageReactionRemoveEvent;
import net.dv8tion.jda.core.exceptions.InsufficientPermissionException;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import javax.security.auth.login.LoginException;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.vdurmont.emoji.EmojiParser;

import alfred.commands.writeMessageCommand;
import alfred.commands.editMessageCommand;
import alfred.commands.getMessageCommand;
import alfred.commands.resetSaveCommand;
import alfred.commands.sampleCommand;
import alfred.commands.setMainServerCommand;
import alfred.commands.setTestServerCommand;

public class alfred extends ListenerAdapter
{	
	public static String IDTestServer = "";
	public static String IDMainServer = ""; 
	public static String adminRole = "";
	public static String ownerID = "";
	public static String token = "";
	//public static List<Map.Entry<String,List<String>>> messagePerServerList = new ArrayList<>();
	//public static Map<String, String> test = new ConcurrentHashMap<String, String>();
	//public static BidiMap<String, String> messageList = new DualHashBidiMap<String, String>();
	public static List<String> messageList = new ArrayList<String>();
	public static List<String> channelList = new ArrayList<String>();
	
    public static void main(String[] args)
    {
        try
        {
        	
        	getData();
        	
			EventWaiter waiter = new EventWaiter();
			CommandClientBuilder client = new CommandClientBuilder();
			client.setOwnerId(ownerID);
			client.setPrefix("!");
			client.useHelpBuilder(false);
		
			client.addCommands(
					new editMessageCommand(),
					new getMessageCommand(),
					new resetSaveCommand(),
					new sampleCommand(),
					new setMainServerCommand(),
					new setTestServerCommand(),
					new writeMessageCommand());
		
			JDA j = new JDABuilder(AccountType.BOT)
					.setToken(token)
					.addEventListener(waiter)
					.addEventListener(new alfred())
					.addEventListener(client.build())
					.build()
					.awaitReady();
			
			
			j.getPresence().setGame(Game.watching("ce server"));
			
			//saveMessageHistory();
			
			getData();
        }
        catch (LoginException e)
        {
            e.printStackTrace();
        }
        catch (InterruptedException e)
        {
        	e.printStackTrace();
        }
    }

    /*
    @Override
    public void onGuildJoin(GuildJoinEvent event)
    {
    	messagePerServerList.put(event.getGuild().getId(), new ArrayList<>());
    }
    */

    @Override
    public void onMessageReactionAdd(MessageReactionAddEvent event)
    {
    	if ((!event.getUser().isBot()) && (messageList.contains(event.getMessageId())))
    	{
    		try
    		{
        		String emote = EmojiParser.parseToAliases(event.getReactionEmote().getName());
    			event.getGuild().getController().addRolesToMember(event.getMember(), event.getGuild().getRolesByName(emote, true)).queue();
    		}
    		catch (InsufficientPermissionException ipe)
    		{
    			event.getUser().openPrivateChannel().queue( (channel) ->
				channel.sendMessage("Bonjour **"+event.getUser().getName() + "**, vous recevez ce message car une erreur est survenue lors d'un évènement d'assignement de rôle.\nIl apparait que je suis dans l'impossibilité d'assigner des rôles sur votre serveur, **"+event.getGuild().getName()+"**.\nMerci de créer un rôle pour moi me permettant d'avoir accès à cette fonctionnalité.\nEn vous remerciant, Alfred.").queue());
    		}
    	}
    }
    
    @Override
    public void onMessageReactionRemove(MessageReactionRemoveEvent event)
    {
    	if (!event.getUser().isBot() && (messageList.contains(event.getMessageId())))
    	{
    		try
    		{
        		String emote = EmojiParser.parseToAliases(event.getReactionEmote().getName());
    			event.getGuild().getController().removeRolesFromMember(event.getMember(), event.getGuild().getRolesByName(emote, true)).queue();
    		}
    		catch (InsufficientPermissionException ipe)
    		{
    			event.getUser().openPrivateChannel().queue( (channel) ->
				channel.sendMessage("Bonjour **"+event.getUser().getName() + "**, vous recevez ce message car une erreur est survenue lors d'un évènement d'assignement de rôle.\nIl apparait que je suis dans l'impossibilité d'assigner des rôles sur votre serveur, **"+event.getGuild().getName()+"**.\nMerci de créer un rôle pour moi me permettant d'avoir accès à cette fonctionnalité.\nEn vous remerciant, Alfred.").queue());
    		}
    	}
    }
        
    @Override
    public void onMessageReceived(MessageReceivedEvent event) {}
    
    
    public static void saveMessageHistory()
	{
		final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
		executorService.scheduleAtFixedRate(new Runnable()
				{
					@Override
					public void run()
					{
						try
						{
							Gson gson = new GsonBuilder().create();
							Writer messageWriter = new FileWriter("messageList.json");							
							gson.toJson(messageList, messageWriter);
							messageWriter.close();
							
							Writer channelWriter = new FileWriter("channelList.json");
							gson.toJson(channelList, channelWriter);
							channelWriter.close();
						}
						catch (IOException e)
						{
				        e.printStackTrace();
						}
					}
				}, 0, 1, TimeUnit.MINUTES);
	}
    
    public static void getData()
    {
    	try
    	{
    		Gson gson = new GsonBuilder().create();

			Reader tokenReader = new FileReader("token.json");
			token = gson.fromJson(tokenReader, String.class);
			tokenReader.close();
			
			Reader ownerIDReader = new FileReader("ownerID.json");
			ownerID = gson.fromJson(ownerIDReader, String.class);
			ownerIDReader.close();
			
			Reader IDTestServerReader = new FileReader("IDTestServer.json");
			IDTestServer = gson.fromJson(IDTestServerReader, String.class);
			IDTestServerReader.close();
			
			Reader IDMainServerReader = new FileReader("IDMainServer.json");
			IDMainServer = gson.fromJson(IDMainServerReader, String.class);
			
			Reader adminRoleReader = new FileReader("adminRole.json");
			adminRole = gson.fromJson(adminRoleReader, String.class);
			adminRoleReader.close();
			
			Reader listMessageReader = new FileReader("messageList.json");
			messageList = gson.fromJson(listMessageReader, new TypeToken<List<String>>(){}.getType());
			listMessageReader.close();
			
			Reader listChannelReader = new FileReader("channelList.json");
			channelList = gson.fromJson(listChannelReader, new TypeToken<List<String>>(){}.getType());
			listChannelReader.close();			
		}
    	catch (IOException e)
    	{
			e.printStackTrace();
		}
    }
    
    public static boolean commandPermission(Member m)
    {
    	boolean permission = false;
    	List<Role> roleList = m.getRoles();
    	for (Role r : roleList)
    	{
    		if (r.getName().equals(adminRole))
    		{
    			permission = true;
    			break;
    		}
    	}
    	return permission;
    }
    
    public static boolean creatorPermission(Member m)
    {
    	boolean permission = false;
    	if (m.getUser().getId().equals(ownerID))
    	{
    		permission = true;
    	}
    	return permission;
    }
    
    public static void JSONsaveTestServer()
    {
		try
		{
			Writer TestServerWriter = new FileWriter("IDTestServer.json");
			Gson gson = new GsonBuilder().create();
			gson .toJson(IDTestServer, TestServerWriter);
			TestServerWriter.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}	
    }
    
    public static void JSONsaveMainServer()
    {
		try
		{
			Writer MainServerWriter = new FileWriter("IDMainServer.json");
			Gson gson = new GsonBuilder().create();
			gson .toJson(IDMainServer, MainServerWriter);
			MainServerWriter.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}	
    }

    public static void JSONsaveListInstant()
    {

		try
		{
			Gson gson = new GsonBuilder().create();
			Writer messageWriter = new FileWriter("messageList.json");							
			gson.toJson(messageList, messageWriter);
			messageWriter.close();
			
			Writer channelWriter = new FileWriter("channelList.json");
			gson.toJson(channelList, channelWriter);
			channelWriter.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
    }
}