package alfred.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

import net.dv8tion.jda.core.Permission;

public class setMainServerCommand extends Command
{
	private boolean publicCommand = false;
	
	public setMainServerCommand()
	{
		this.name = "setMain";
		this.help = "[TEST SERVER ONLY] set le serveur actif";
		this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS};
		this.guildOnly = false;
	}

	@Override
	protected void execute(CommandEvent event) {
		try
		{
			if (event.getGuild().getId().equals(alfred.alfred.IDTestServer))
			{
				onTestBranch(event);
			}
			else
			{
				if (this.publicCommand)
				{
					onPublicBranch(event);
				}	
			}
				
		}
		catch (java.lang.NullPointerException e)
		{
			if (this.publicCommand)
			{
				onPublicBranch(event);
			}
		}
	}

	
	protected void onPublicBranch(CommandEvent event) {
		
	}
	
	protected void onTestBranch(CommandEvent event) {
		if (alfred.alfred.creatorPermission(event.getMember()))
		{
			alfred.alfred.IDMainServer = event.getGuild().getId();
			alfred.alfred.JSONsaveMainServer();
			event.reply("Le serveur "+event.getGuild().getName()+" est désormais le serveur principal, "+event.getMember().getEffectiveName());
		}
		else
		{
			event.reply("Mes excuses "+event.getMember().getEffectiveName()+", mais seul un développeur peut utiliser cette commande");
		}
	}
}