package alfred.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Message;

public class getMessageCommand extends Command
{
	private boolean publicCommand = false;
	
	public getMessageCommand()
	{
		this.name = "get";
		this.help = "[TEST SERVER ONLY] [n° du message]";
		this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS};
		this.guildOnly = false;
	}

	@Override
	protected void execute(CommandEvent event) {
		try
		{
			if (event.getGuild().getId().equals(alfred.alfred.IDTestServer))
			{
				onTestBranch(event);
			}
			else
			{
				if (this.publicCommand)
				{
					onPublicBranch(event);
				}	
			}
				
		}
		catch (java.lang.NullPointerException e)
		{
			if (this.publicCommand)
			{
				onPublicBranch(event);
			}
		}
	}

	
	protected void onPublicBranch(CommandEvent event) {
		
	}
	
	protected void onTestBranch(CommandEvent event) {
		if (alfred.alfred.commandPermission(event.getMember()))
		{
			String[] arg = event.getArgs().split("\\s+");
			/*
			 * TODO : renvoyer la liste de tout les messages, par paquet de 10
			*/
			try {
				if (arg[0].equals("")) {
					event.reply(event.getAuthor().getName()+", veuillez utilisez cette commande comme suit : get [n° du message]");
				}
				else {
					String channelId = alfred.alfred.channelList.get(Integer.parseInt(arg[0])-1);
					String messageId = alfred.alfred.messageList.get(Integer.parseInt(arg[0])-1);
					Message m = event.getGuild().getTextChannelById(channelId).getMessageById(messageId).complete();
					event.reply(m.getContentRaw());
					event.reply(m.getReactions().toString());
				}
			}
			catch (IndexOutOfBoundsException e) {
				event.reply("Veuillez rentrer un chiffre valide\nA titre d'information, j'ai actuellement écrit **"+alfred.alfred.messageList.size()+"** messages sur ce serveur\nVeuillez donc rentrer un chiffre entre 1 et "+alfred.alfred.messageList.size());
			}
		}		
	}	
}