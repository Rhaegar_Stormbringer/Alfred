package alfred.commands;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

import net.dv8tion.jda.core.Permission;

public class setTestServerCommand extends Command
{
	private boolean publicCommand = false;
	
	public setTestServerCommand()
	{
		this.name = "setTest";
		this.help = "[TEST SERVER ONLY] set le serveur de test";
		this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS};
		this.guildOnly = false;
	}

	@Override
	protected void execute(CommandEvent event) {
		try
		{
			if (event.getGuild().getId().equals(alfred.alfred.IDTestServer))
			{
				onTestBranch(event);
			}
			else
			{
				if (this.publicCommand)
				{
					onPublicBranch(event);
				}	
			}
				
		}
		catch (java.lang.NullPointerException e)
		{
			if (this.publicCommand)
			{
				onPublicBranch(event);
			}
		}
	}

	
	protected void onPublicBranch(CommandEvent event) {
		
	}
	
	protected void onTestBranch(CommandEvent event) {
		if (alfred.alfred.creatorPermission(event.getMember()))
		{
			alfred.alfred.IDTestServer = event.getGuild().getId();
			alfred.alfred.JSONsaveTestServer();
			event.reply("Le serveur "+event.getGuild().getName()+" est désormais le serveur de test, "+event.getMember().getEffectiveName());
		}
		else
		{
			event.reply("Mes excuses "+event.getMember().getEffectiveName()+", mais seul un développeur peut utiliser cette commande");
		}
	}
}