package alfred.commands;

import java.util.ArrayList;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

import net.dv8tion.jda.core.Permission;

public class resetSaveCommand extends Command
{
	private boolean publicCommand = false;
	
	public resetSaveCommand()
	{
		this.name = "resetSave";
		this.help = "[TEST SERVER ONLY] supprime la sauvegarde actuelle des messages";
		this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS};
		this.guildOnly = false;
	}

	@Override
	protected void execute(CommandEvent event) {
		try
		{
			if (event.getGuild().getId().equals(alfred.alfred.IDTestServer))
			{
				onTestBranch(event);
			}
			else
			{
				if (this.publicCommand)
				{
					onPublicBranch(event);
				}	
			}
				
		}
		catch (java.lang.NullPointerException e)
		{
			if (this.publicCommand)
			{
				onPublicBranch(event);
			}
		}
	}

	
	protected void onPublicBranch(CommandEvent event) {
		
	}
	
	protected void onTestBranch(CommandEvent event) {
		if (alfred.alfred.creatorPermission(event.getMember()))
		{
			alfred.alfred.messageList = new ArrayList<String>();
			alfred.alfred.channelList = new ArrayList<String>();
			alfred.alfred.JSONsaveListInstant();
			event.reply("Sauvegarde supprimée, "+event.getMember().getEffectiveName());
		}
		else
		{
			event.reply("Mes excuses "+event.getMember().getEffectiveName()+", mais seul un développeur peut utiliser cette commande");
		}
	}
}