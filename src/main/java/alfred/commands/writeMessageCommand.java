package alfred.commands;

import java.util.ArrayList;
import java.util.List;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.vdurmont.emoji.EmojiParser;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Emote;

public class writeMessageCommand extends Command
{
	private boolean publicCommand = false;
	
	public writeMessageCommand()
	{
		this.name = "write";
		this.help = "[TEST SERVER ONLY] [salon] [contenu du message] [£ emotes (optionnel)]";
		this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS};
		this.guildOnly = false;
	}

	@Override
	protected void execute(CommandEvent event) {
		try
		{
			if (event.getGuild().getId().equals(alfred.alfred.IDTestServer))
			{
				onTestBranch(event);
			}
			else
			{
				if (this.publicCommand)
				{
					onPublicBranch(event);
				}	
			}
				
		}
		catch (java.lang.NullPointerException e)
		{
			if (this.publicCommand)
			{
				onPublicBranch(event);
			}
		}
	}

	
	protected void onPublicBranch(CommandEvent event) {
		
	}
	
	protected void onTestBranch(CommandEvent event) {
		if (alfred.alfred.commandPermission(event.getMember()))
		{
			/*
			 * PART 1 : split the command to retrieve arguments : channel, content of the message, and list of emoji
			 */
			String[] arg = event.getArgs().split("\\s+");
			int i, j = 0;
			String channel = arg[0].replace("<", "").replace(">", "").replace("#", "");
			String message = arg[1].replace("$", "")+" ";
			List<String> emote = new ArrayList<>();
		
			for (i=2; i < arg.length; i ++)
			{
				if (arg[i].contains("£"))
				{
					break;
				}
				message += arg[i];
				if (i != arg.length-1) {
					message += " ";
				}
			}	
		
			/*
			* little tricky for the emojis : the Emoji-java API throws 2 outputs formats when using EmojiParser#parseToAliases :
			* 			- ":name:" if the emoji is a supported emoji
			* 			- "<:name:1234567890>" if the emoji is unknow (with 1234567890 representing the emoji's id, which could not be this number)
			* So to retrieve the id of the custom emoji, a regex expression is used, as long as a final replacement
			*/
			boolean check1 = false;
			for (j=i+1; j < arg.length	; j++)
			{
				List<Emote> guildE = event.getGuild().getEmotes();
				for (Emote e : guildE)
				{
					if (e.getId().equals(EmojiParser.parseToAliases(arg[j].replaceAll("<:([a-zA-Z0-9])*:", "").replace(">", ""))))
					{
						emote.add(EmojiParser.parseToAliases(arg[j].replaceAll("<:([a-zA-Z0-9])*:", "").replace(">", "")));
						check1 = true;
						break;
					}
				}
				if (!check1)
				{
					emote.add(EmojiParser.parseToAliases(arg[j]));
				}
				check1 = false;
			}
		
			/*
			 * PART 2 : Retrieve real channel, construction and sending of the final message (with all the required reactions), and addition to the
			 * list of message
			 */
			event.getGuild().getTextChannelById(channel).sendMessage("@everyone    ["+(alfred.alfred.messageList.size()+1)+"]\n"+message).queue(
					msg->{
						boolean check2 = false;
						List<Emote> guildEmote = event.getGuild().getEmotes();
						for (String s : emote)
						{
							for (Emote e : guildEmote)
							{
								if (e.getId().equals(s))
								{
									msg.addReaction(event.getGuild().getEmoteById(e.getId())).queue();
									check2 = true;
								}
							}
							if (!check2)
							{
								msg.addReaction(EmojiParser.parseToUnicode(s)).queue();
							}
							check2 = false;
						}
						if (event.getGuild().getId().equals(alfred.alfred.IDMainServer))
						{
							alfred.alfred.messageList.add(msg.getId());
							alfred.alfred.channelList.add(channel);	
						}
						//alfred.alfred.messageList.put(channel, msg.getId());
					});
		}
	}
}