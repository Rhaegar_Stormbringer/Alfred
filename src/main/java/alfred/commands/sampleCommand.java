package alfred.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

import net.dv8tion.jda.core.Permission;

public class sampleCommand extends Command
{
	private boolean publicCommand = false;
	
	public sampleCommand()
	{
		this.name = "sample";
		this.help = "[TEST SERVER ONLY] sample for all commands";
		this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS};
		this.guildOnly = false;
	}

	@Override
	protected void execute(CommandEvent event) {
		try
		{
			if (event.getGuild().getId().equals(alfred.alfred.IDTestServer))
			{
				onTestBranch(event);
			}
			else
			{
				if (this.publicCommand)
				{
					onPublicBranch(event);
				}	
			}
				
		}
		catch (java.lang.NullPointerException e)
		{
			if (this.publicCommand)
			{
				onPublicBranch(event);
			}
		}
	}

	
	protected void onPublicBranch(CommandEvent event) {
		
	}
	
	protected void onTestBranch(CommandEvent event) {
		event.reply("This is a sample command");
		event.reply("If you can read this, you're on the Test Server");
	}
	
}