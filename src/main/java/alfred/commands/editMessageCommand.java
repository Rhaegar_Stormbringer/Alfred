package alfred.commands;

import java.util.ArrayList;
import java.util.List;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.vdurmont.emoji.EmojiParser;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Emote;
import net.dv8tion.jda.core.entities.Message;

public class editMessageCommand extends Command
{
	private boolean publicCommand = false;
	
	public editMessageCommand()
	{
		this.name = "edit";
		this.help = "[TEST SERVER ONLY] [n° du message] [nouveau contenu du message (optionnel)] [£ NOUVELLES emotes (optionnel)]";
		this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS};
		this.guildOnly = false;
	}

	@Override
	protected void execute(CommandEvent event) {
		try
		{
			if (event.getGuild().getId().equals(alfred.alfred.IDTestServer))
			{
				onTestBranch(event);
			}
			else
			{
				if (this.publicCommand)
				{
					onPublicBranch(event);
				}	
			}
				
		}
		catch (java.lang.NullPointerException e)
		{
			if (this.publicCommand)
			{
				onPublicBranch(event);
			}
		}
	}

	
	protected void onPublicBranch(CommandEvent event) {
		
	}
	
	protected void onTestBranch(CommandEvent event) {
		/*
		 * checking for permission to use commands
		 */
		if (alfred.alfred.commandPermission(event.getMember()))
		{
			/*
			 * PART 1 : split the command to retrieve arguments : id of the message, content of the message, and list of emoji
			 */
			String[] arg = event.getArgs().split("\\s+");
			int i, j = 0;
			int ID = Integer.parseInt(arg[0])-1;
			String newContent = "";
			List<String> emote = new ArrayList<>();
			if (arg.length > 1)
			{
				//newContent = arg[1].replace("$", "")+" ";
				for (i=1; i < arg.length; i ++)
				{
					if (arg[i].contains("£"))
					{
						break;
					}
					newContent += arg[i];
					if (i != arg.length-1) {
						newContent += " ";
					}
				}
				
				/*
				 * little tricky for the emojis : the Emoji-java API throws 2 outputs formats when using EmojiParser#parseToAliases :
				 * 			- ":name:" if the emoji is a supported emoji
				 * 			- "<:name:1234567890>" if the emoji is unknow (with 1234567890 representing the emoji's id, which could not be this number)
				 * So to retrieve the id of the custom emoji, a regex expression is used, as long as a final replacement
				 */
				boolean check1 = false;
				for (j=i+1; j < arg.length	; j++)
				{
					List<Emote> guildE = event.getGuild().getEmotes();
					for (Emote e : guildE)
					{
						if (e.getId().equals(EmojiParser.parseToAliases(arg[j].replaceAll("<:([a-zA-Z0-9])*:", "").replace(">", ""))))
						{
							emote.add(EmojiParser.parseToAliases(arg[j].replaceAll("<:([a-zA-Z0-9])*:", "").replace(">", "")));
							check1 = true;
							break;
						}
					}
					if (!check1)
					{
						emote.add(EmojiParser.parseToAliases(arg[j]));
					}
					check1 = false;
				}
			}
			
			/*
			 * PART 2 : retrieve the real message
			 */
			String channelId = alfred.alfred.channelList.get(ID);
			String messageId = alfred.alfred.messageList.get(ID);
			Message m = event.getGuild().getTextChannelById(channelId).getMessageById(messageId).complete();
			
			/*
			 * PART 3 : modification of the content and / or the emojis of the real message
			 */
			if (!newContent.equals(""))
			{
				m.editMessage("@everyone    ["+(ID+1)+"]\n"+newContent).queue();	
			}
			boolean check2 = false;
			List<Emote> guildEmote = event.getGuild().getEmotes();
			for (String s : emote)
			{
				for (Emote e : guildEmote)
				{
					if (e.getId().equals(s))
					{
						m.addReaction(event.getGuild().getEmoteById(e.getId())).queue();
						check2 = true;
					}
				}
				if (!check2)
				{
					m.addReaction(EmojiParser.parseToUnicode(s)).queue();
				}
			check2 = false;
			}
		}
	}
}