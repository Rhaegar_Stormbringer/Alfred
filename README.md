# Alfred (Java Discord Bot)

Discord Bot of **Centrifuge - Game Lab**.

**Centrifuge - Game Lab** is a club focused on game creation (with a main focus
on video games), created by 3 students of the INSA Toulouse in January 2019.
In addition to help its members to meet and work together on personnal projects,
the club also provide somes sessions about Basics of Game Development, as well
as a VR workstation.

## Features:
Alfred is the Bot developped for this club and its Discord server to automate
role management for its members. The Bot is able to create messages with the
'everyone' mention and a list of reactions that users can use to gain the roles
they want. It is also able to edit these messages. Eventually, others
features may be developped in the future.

## Dependencies:
Alfred was developped in Java and rely mainly on two API : **JDA** and
**JDA-Utilities**. It also use two more API, **Gson** for data storage, and
**Emoji-Java** for emoji recognition.

This project requires **Java 8**.<br>
All dependencies are managed automatically by Gradle.
 * JDA
   * Version: **2.1.5**
   * [Github](https://github.com/DV8FromTheWorld/JDA)
   * [JCenter Repository](https://bintray.com/dv8fromtheworld/maven/JDA/4.ALPHA.0_65)
 * JDA-utilities
   * Version: **3.13.0**
   * [Github](https://github.com/JDA-Applications/JDA-Utilities)
   * [JCenter Repository](https://bintray.com/jagrosh/maven/JDA-Utilities/2.1.5)
 * Gson
   * Version: **2.8.5**
   * [Github](https://github.com/google/gson)
   * [JCenter Repository](https://bintray.com/bintray/jcenter/com.google.code.gson%3Agson)
 * Emoji-java
   * Version: **4.0.0**
   * [Github](https://github.com/vdurmont/emoji-java)
   * [JCenter Repository](https://bintray.com/bintray/jcenter/com.vdurmont%3Aemoji-java)



------

# Alfred (Bot Discord Java)

Bot Discord de **Centrifuge - Game Lab**.

**Centrifuge - Game Lab** est un club centré sur la création de jeux (vidéo 
principalement), créé par 3 étudiants de l'INSA de Toulouse en Janvier 2019.
En plus de proposer à ses membres un cadre pour se rencontrer et travailler
ensemble sur leur projet, le club propose également des initiations aux
fondamentaux du développement de jeux, ainsi qu'un poste de travail pour la VR.

## Fonctionnalités:
Alfred est un Bot développé pour ce club et son serveur Discord afin
d'automatiser la gestion des rôles pour ses membres. Le Bot est capable de
créer des messages avec la mention 'everyone' et une liste d'emoji en réaction
que les utilisateurs peuvent utiliser pour gagner le(s) rôle(s) de leur choix.
Il est également capable d'éditer ces messages. Eventuellement, d'autres
fonctionnalités pourront être développés dans le futur.

## Dépendances:
Alfred est développé en Java et s'appuie majoritairement sur deux API : **JDA** et
**JDA-Utilities**. Il utilise également deux API supplémentaires, **Gson** pour le 
stockage de données, et **Emoji-Java** pour la reconnaissance d'emoji.

Ce projet nécessite **Java 8**.<br>
Toutes les dépendances sont gérées automatiquement par Gradle.
 * JDA
   * Version: **2.1.5**
   * [Github](https://github.com/DV8FromTheWorld/JDA)
   * [JCenter Repository](https://bintray.com/dv8fromtheworld/maven/JDA/4.ALPHA.0_65)
 * JDA-utilities
   * Version: **3.13.0**
   * [Github](https://github.com/JDA-Applications/JDA-Utilities)
   * [JCenter Repository](https://bintray.com/jagrosh/maven/JDA-Utilities/2.1.5)
 * Gson
   * Version: **2.8.5**
   * [Github](https://github.com/google/gson)
   * [JCenter Repository](https://bintray.com/bintray/jcenter/com.google.code.gson%3Agson)
 * Emoji-java
   * Version: **4.0.0**
   * [Github](https://github.com/vdurmont/emoji-java)
   * [JCenter Repository](https://bintray.com/bintray/jcenter/com.vdurmont%3Aemoji-java)
